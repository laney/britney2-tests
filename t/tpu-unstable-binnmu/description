Test for newer binNMU in t-p-u than unstable

This test tests the combination of a binNMU in testing-proposed-updates and
unstable, with a higher binNMU version in t-p-u.

When both the binNMUs are candidates, the result depends on the order in which
they are tried. If the binNMU from unstable (with the lower version) is tried
after the binNMU from t-p-u is accepted, the version will go backwards.

This test has a force-hint for the binNMU in t-p-u, to make sure that is
processed first. The force hint doesn't actually force anything, it just
changes the order.

There is a variant without a force-hint. In this case the order is undefined
and the result depends on the (random) order. This variant might pass or fail,
depending on this order, which might change due to unrelated changes.
Currently (2018-12-17) the dependency for the binNMU from unstable on a binary
from another source makes britney try the binNMU after the binNMU from t-p-u.

This test is based on tpu-with-unstable-binnmu, with a newer binNMU in t-p-u
instead of a newer source.

Please note that the situation of this test should not happen: having a higher
version in t-p-u or testing than in unstable is something that normally
shouldn't happen. A binNMU stable-proposed-updates (destined for stable) could
have a higher version, but that doesn't happen very often.

