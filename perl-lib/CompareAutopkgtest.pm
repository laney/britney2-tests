#! /usr/bin/perl

# Copyright 2018-2020 Ivo De Decker <ivodd@debian.org>
# License GPL-2 or (at your option) any later.

package CompareAutopkgtest;

use strict;
use warnings;
use autodie;

use Data::Dumper;
use YAML::Syck;

use Exporter qw(import);

our @EXPORT = qw(compare_debci get_debci_issues);

my $issues = ();
my $ok = 1;

sub add_issue {
    my $issue = shift;

    push @$issues, $issue;
    $ok = 0;
}

sub load_debci_info {
    my $file = shift;
    my $t_info = {};

	open(my $handle, "<", $file);
	chomp(my @lines = <$handle>);
	close($handle);

    return map {$_ => 1} @lines;
}

sub compare_debci {
    my $debci1 = shift;
    my $debci2 = shift;

    $issues = ();
    $ok = 1;

    my %t1_info = load_debci_info($debci1);
    my %t2_info = load_debci_info($debci2);

	foreach my $k (sort keys %t1_info) {
		unless (defined $t2_info{$k}) {
			$ok = 0;
			add_issue("not in debci2: $k");
		}
	}
	
	foreach my $k (sort keys %t2_info) {
		unless (defined $t1_info{$k}) {
			$ok = 0;
			add_issue("not in debci1: $k");
		}
	}
    return $ok;
}

sub get_debci_issues {
    return $issues;
}


1;
